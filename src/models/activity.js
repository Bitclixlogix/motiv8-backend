const mongoose = require('mongoose')
const constants = require('../constants');

const { Schema } = mongoose;

const activitySchema = new Schema({
    user_id: { type: Schema.Types.ObjectId, required: false, ref: 'user' },
    type: { type: String, enum: ['Walking', 'Running'] },
    distance: { type: Number },
    time: { type: String },
    steps: { type: Number },
    calories: { type: Number },
    starttime: { type: String },
    endtime: { type: String },
    startLocation: [{ type: Number }],
    endLocation: [{ type: Number }],
    weight: { type: Number, default: 0 },
    height: { type: Number, default: 0 },
}, { timestamps: true });

activitySchema.index({ "startLocation": "2dsphere" });
activitySchema.index({ "endLocation": "2dsphere" });

module.exports = mongoose.model('activity', activitySchema);