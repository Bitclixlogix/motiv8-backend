const mongoose = require('mongoose')
const constants = require('../constants');
const bcrypt = require('bcrypt');

const { Schema } = mongoose;

const userSchema = new Schema({
    name: { type: String, default: "" },
    email: { type: String, default: "" },
    mobileNo: { type: String, required: true, unique: true },
    profileImage: { type: String, default: constants.defaultProfile },
    weight: { type: Number, default: 0 },
    height: { type: Number, default: 0 },
    dob: { type: Date, default: "" },
    gender: { type: String, enum: ['Male', 'Female', 'Others'], default: "Male" },
    userType: { type: String, enum: ['User', 'Admin'], default: 'User' },
    status: { type: String, enum: ['Active', 'Inactive'], default: "Active" },
    isProfileSetupCompleted: { type: Boolean, default: false },
    isOtpVerified: { type: Boolean, default: false },
    password: { type: String }
}, { timestamps: true });

userSchema.methods.hash = password => bcrypt.hashSync(password, constants.saltRounds);

userSchema.methods.compare = (password, hash) => bcrypt.compareSync(password, hash);

module.exports = mongoose.model('user', userSchema, 'users');


(function init() {
    let obj = {
        userType: 'Admin',
        email: "ajit@clixlogix.com",
        mobileNo: '9999999999',
        password: bcrypt.hashSync('123456', constants.saltRounds)
    };
    mongoose.model('user', userSchema,  'users').findOne({userType: 'Admin'}, (err, result) => {
        if (err) console.log("Admin creation at findOne error--> ", err);
        else if (!result) {
            mongoose.model('user', userSchema, 'users').create(obj, (err, success) => {
            })
        } else {
            console.log("Admin.");
        }
    })
 })
 ();
