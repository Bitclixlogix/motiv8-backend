const mongoose = require('mongoose');
const constants = require('../constants');
const moment = require('moment');

const { Schema } = mongoose;

const schema = new Schema({
    userid: { type: Schema.Types.ObjectId, ref: 'user' },
    adminid: { type: Schema.Types.ObjectId, ref: 'admin' },
    otp: { type: Number, default: 7777 },
    type: { type: String, enum: ['User', 'Admin'], default: null }
}, { timestamps: true });

schema.methods.isExpired = async function (_id, type) {
    let otp;
    if (type == 'Admin') {
        otp = await this.model('otp').findOne({ adminid: _id });
    } else {
        otp = await this.model('otp').findOne({ userid: _id });
    }
    let expiresAt = moment(otp.updatedAt).add(constants.otpExpiresIn, 'ms');
    let expired = await moment().isSameOrAfter(expiresAt);
    if (expired) {
        return true;
    } else {
        return false;
    }
}

module.exports = mongoose.model('otp', schema);
