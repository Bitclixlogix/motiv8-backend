const mongoose = require('mongoose');
let Schema = mongoose.Schema;

const cmsPageSchema = new Schema({
  pageName: { type: String, required: true, unique: true },
  pageContent: { type: String, required: true },
  status: { type: String, enum: ['active', 'inactive'] },
}, { timestamps: true });

module.exports = mongoose.model('cmspage', cmsPageSchema);


