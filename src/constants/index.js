const self = module.exports = {
    saltRounds: 12,
    jwtSecret: '5ebe2294ecd0e0f08eab7690d2a6ee69',
    tokenExpiresIn: 7 * 24 * 60 * 60,
    get refreshTokenExpiresIn() {
        return 24 * 60 * self.tokenExpiresIn
    },
    otpExpiresIn: 1 * 5 * 60 * 1000,
    msgTitle: 'MotiV8',
    defaultProfile: 'user-default-profile-pic.jpg',
};