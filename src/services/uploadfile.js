const path = require('path');
const multer = require('multer');
const fs = require('fs')

var storage = multer.diskStorage({
    destination: function (req, file, next) {
        var dir; var pathToFile;
        if (file.fieldname === 'profileimage') {
            dir = path.dirname(require.main.filename) + "/upload/profileimage";
            pathToFile = "upload/profileimage";
        } 
        if (!fs.existsSync(dir)) {
            var dirName = path.dirname(require.main.filename);
            var filePathSplit = pathToFile.split('/');
            for (var index = 0; index < filePathSplit.length; index++) {
                dirName += '/' + filePathSplit[index]
                if (!fs.existsSync(dirName)) {
                    fs.mkdirSync(dirName, '0777');
                    fs.chmodSync(dirName, '777');
                }
            }
            next(null, dir)
        } else {
            next(null, dir)
        }
    },
    filename: function (req, file, next) {
        const ext = file.mimetype.split('/')[1];
        const filename = file.originalname.substr(0, file.originalname.lastIndexOf('.')) + '_' + new Date().getTime() + '.' + ext;
        next(null, filename)
    }
})

exports.upload = multer({ storage: storage, limits: { fileSize: 10 * 1024 * 1024 } })

exports.validate = (req, res, next) => {
    if (!req.file) {
        return res
            .status(500)
            .send({ success: false, message: "file can't be empty" })
    }
    next();
}