const express = require('express');
const router = express.Router();
const activityUseCase = require('../../controllers/activity');
const auth = require('../../services/auth');

router
    .post('/', auth.authenticate, (req, res) => {
        activityUseCase
            .create_activity({ userid: req.decoded.id, body: req.body })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })

router
    .get('/activitycount', auth.authenticate, (req, res) => {
        activityUseCase
            .get_all_activities_count({ userid: req.decoded.id })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })

router
    .get('/details/:id', auth.authenticate, (req, res) => {
        activityUseCase
            .getOne_activity({ id: req.params.id })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })




module.exports = router    