const express = require('express');
const userController = require('../../controllers/user');
const router = express.Router();
const { Success, Fail } = require('../../services/response')
const uploadFile = require('../../services/uploadfile');
const path = require('path');
const auth = require('../../services/auth');

router
    .post('/mobileverify', (req, res) => {
        userController
            .mobile_verify({ body: req.body })
            .then((data) => {
                res.send({ "statusCode": 200, data: data, message: 'OTP has been sent to the user mobile.' })
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })

router
    .post('/', (req, res) => {
        userController
            .create_user({ body: req.body })
            .then((data) => {
                res.status(200).send(data)
                // res.json(Success(data, 'User created Successfully'))
            })
            .catch((error) => {
                if (error.message.includes("ValidationError")) {
                    res.json(Fail(error.message.split(',')))
                } else {
                    res.status(400);
                    res.send({ "statusCode": 400, message: error.message })
                    // res.json(Fail(error.message))
                }
            })
    })

router
    .post('/:id', uploadFile.upload.single('profileimage'), (req, res) => {
        userController
            .updateProfile({ id: req.params.id, body: req.body, file: req.file })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                if (error.message.includes("ValidationError")) {
                    res.json(Fail(error.message.split(',')))
                } else {
                    res.status(400);
                    res.send({ "statusCode": 400, message: error.message })
                }
            })
    })

router
    .get('/profileimage', (req, res) => {
        let { img_name } = req.query;
        const dir = path.join(path.dirname(require.main.filename), './upload/profileimage')
        var imgpath = dir + '/' + img_name;
        res.sendFile(imgpath);
    })

router
    .get('/allusers', (req, res) => {
        userController
            .getall_users({ query: req.query })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })

router
    .get('/details/:id', (req, res) => {
        userController
            .get_user_details({ id: req.params.id })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })

module.exports = router