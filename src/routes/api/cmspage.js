const express = require('express');
const cmsController = require('../../controllers/cmspage');
const router = express.Router();
const { Success, Fail } = require('../../services/response')

router
    .post('/createandupdate', (req, res) => {
        cmsController
            .createAndUpdate({ body: req.body })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })

router
    .get('/cmspage/:pageName', (req, res) => {
        cmsController
            .getOne({ params: req.params })
            .then((data) => {
                res.json(Success(data, 'CMS Page returned Successfully'))
            })
            .catch((error) => {
                 res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })

router
    .get('/', (req, res) => {
        cmsController
            .get({ query: req.query })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })

router
    .delete('/:id', (req, res) => {
        cmsController
            .delete({ id: req.params.id })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })


module.exports = router