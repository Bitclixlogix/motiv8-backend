const express = require('express');
const router = express.Router();
const authUseCase = require('../../controllers/auth');
const { Success, Fail } = require('../../services/response')

router
    .post('/otpVerify', (req, res) => {
        authUseCase
            .otpVerify({ body: req.body })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })

router
    .post('/login', (req, res) => {
        authUseCase
            .login({ body: req.body })
            .then((data) => {
                res.status(200).send(data)
            })
            .catch((error) => {
                res.status(400);
                res.send({ "statusCode": 400, message: error.message })
            })
    })

router
    .post('/adminlogin', (req, res) => {
        authUseCase
            .adminlogin({ body: req.body })
            .then((data) => {
                res.json(Success(data, "Admin login successfully"))
            })
            .catch((error) => {
                res.json(Fail(error.message))
            })
    })


module.exports = router