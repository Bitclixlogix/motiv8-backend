const express = require('express');
const userRoutes = require('./api/user');
const authRoutes = require('./api/auth');
const cmsRoutes = require('./api/cmspage');
const activityRoutes = require('./api/activity');

const router = express.Router();
router.use('/user', userRoutes);
router.use('/auth', authRoutes);
router.use('/cms', cmsRoutes);
router.use('/activity', activityRoutes);

router.get('/', (req, res) => res.json('Welcome to MotiV8 APIs.'));

module.exports = router;