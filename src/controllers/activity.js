const activityModel = require('../models/activity')
var mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;


exports.create_activity = ({ userid, body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let activity = new activityModel({
                user_id: userid,
                type: body.type,
                distance: body.distance,
                time: body.time,
                steps: body.steps,
                calories: body.calories,
                starttime: body.starttime,
                endtime: body.endtime,
                startLocation: body.startLocation,
                endLocation: body.endLocation,
                weight: body.weight,
                height: body.height
            })
            let activitydata = await activity.save()
            if (!activitydata || activitydata == null) {
                resolve({ success: false, message: 'User not added.' })
            } else {
                resolve({ success: true, data: activitydata, message: 'Activity created successfully' });
            }
        } catch (err) {
            reject(err);
        }
    });
}

exports.getOne_activity = ({ id }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let product = await activityModel.findById(ObjectId(id));
            if (!product || product == null) {
                resolve({ success: false, message: 'Activity does not exist.' })
            } else {
                resolve({ success: true, data: product, message: 'Activity returned successfully' });
            }
        } catch (err) {
            reject(err);
        }
    });
}

exports.get_all_activities_count = ({ userid }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let products = await activityModel.aggregate([{ $match: { user_id: ObjectId(userid) } }, { $group: { _id: 0, totaldistance: { $sum: "$distance" }, totalsteps: { $sum: "$steps" }, totalcalories: { $sum: "$calories" } } }]);
            if (products && products.length == 0) {
                resolve({ success: true, data: { totaldistance: 0, totalsteps: 0, totalcalories: 0 }, message: 'Activity status does not exist.' })
            } else {
                resolve({ success: true, data: products[0], message: 'Activity status returned successfully' });
            }
        } catch (err) {
            reject(err);
        }
    });
}