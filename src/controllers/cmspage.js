const CMS = require('../models/cms-page')
var mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;

module.exports.createAndUpdate = ({ body }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let _content = await CMS.findOne({ pageName: { '$regex': body.pageName, '$options': 'i' } });
      if (_content) {
        let data = await CMS.findByIdAndUpdate(_content._id, body, { new: true });
        resolve({ success: true, data, message: 'Page updated.' });
      }
      else {
        let data = await CMS.create(body);
        resolve({ success: true, data, message: 'Page created.' });
      }
    } catch (err) {
      reject(err)
    }
  })
}

module.exports.getOne = ({ params }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let { pageName, status } = params;
      let query = {
        ...(pageName && { pageName: { $regex: ".*" + pageName + ".*", $options: 'i' } }),
        ...(status && { status: 'Active' }),
      }
      let cmspage = await CMS.findOne(query);
      resolve(cmspage)
    } catch (err) {
      reject(err)
    }
  })
}

module.exports.delete = ({ id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      let data = await CMS.findByIdAndRemove(ObjectId(id));
      resolve({ success: true, data, message: 'Page deleted.' });
    } catch (err) {
      reject(err);
    }
  })
}

module.exports.get = ({ query }) => {
  return new Promise(async (resolve, reject) => {
    let { page, count, order, sort, pageName, status } = query;
    let skip = page * count - count || 0;
    let limit = parseInt(count) || 10;
    order = order == "desc" ? -1 : 1;
    sort = { [sort || 'createdAt']: order };
    try {
      let query = {
        ...(pageName && { pageName: { $regex: ".*" + pageName + ".*", $options: 'i' } }),
        ...(status && { status }),
      }
      let _count = await CMS.countDocuments();
      let data = await CMS.find(query)
        .select('pageName pageContent createdAt status')
        .sort(sort)
        .skip(skip)
        .limit(limit);
      resolve({ success: true, data, count: _count, message: 'All pages fetched.' });
    } catch (err) {
      reject(err);
    }
  })
}



