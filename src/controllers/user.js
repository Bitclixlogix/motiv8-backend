const UserModel = require('../models/user')
const OTPModel = require('../models/otp')
var client = require('twilio')(
    process.env.TWILIO_ACCOUNT_SID,
    process.env.TWILIO_AUTH_TOKEN
);
var mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;

module.exports.mobile_verify = ({ body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            const userCredentials = await UserModel.findOne({ $and: [{ 'mobileNo': body.mobileNo }, { userType: 'User' }, { status: 'Active' }] }, { firstName: 1, lastName: 1, mobileNo: 1, status: 1 })
            if (userCredentials) {
                throw new Error('User already exists !')
            } else {
                console.log('Mobile :------>', process.env.TWILIO_PHONE_NUMBER);
                client.messages.create({
                    from: process.env.TWILIO_PHONE_NUMBER,
                    to: body.mobileNo,
                    body: "You just sent an SMS from Node.js using Twilio!"
                })
                    .then((message) => {
                        console.log('message :---->>', message.sid);
                    })
                    .catch((error) => {
                        console.log('Error for ems :---->>', error);
                    })
                let model = new OTPModel({ mobileNo: body.mobileNo, type: 'User' });
                await model.save();
                resolve(userCredentials)
            }
        } catch (err) {
            reject(err)
        }
    })
}

module.exports.create_user = async ({ body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let isUserExist = await UserModel.findOne({ $and: [{ 'mobileNo': body.mobileNo }, { userType: 'User' }, { status: 'Active' }] }, { name: 1, mobileNo: 1, status: 1, isOtpVerified: 1 })
            if (isUserExist != null && !isUserExist.isOtpVerified) {
                let check = await OTPModel.findOne({ $and: [{ userid: isUserExist._id }, { type: 'User' }] });
                if (check) {
                    check.createdAt = new Date();
                    await check.save();
                } else {
                    let model = new OTPModel({ userid: isUserExist._id, type: 'User' });
                    await model.save();
                }
                response = { success: true, message: 'OTP sent successfully.' };
            } else if (isUserExist != null && isUserExist.isOtpVerified) {
                response = { success: false, message: 'User already registered.' };
            }
            else {
                let user = new UserModel({
                    mobileNo: body.mobileNo
                })
                let userCredentials = await user.save()
                if (!userCredentials || userCredentials == null) {
                    response = { success: true, message: 'User not added.' };
                } else {
                    let model = new OTPModel({ userid: userCredentials._id, type: 'User' });
                    await model.save();
                    response = { success: true, message: 'OTP sent to your registered mobile number' };
                }
            }
            resolve(response)
        } catch (err) {
            reject(err)
        }
    })
}


module.exports.updateProfile = async ({ id, body, file }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let response = {}
            if (id.match(/^[0-9a-fA-F]{24}$/)) {
                let data = {
                    name: body.name,
                    email: body.email,
                    mobileNo: body.mobileNo,
                    weight: body.weight,
                    height: body.height,
                    dob: body.dob,
                    gender: body.gender,
                    isProfileSetupCompleted: true,
                }
                var new_data = {};
                if (file) {
                    new_data = Object.assign({ profileImage: file.filename }, data);
                } else {
                    new_data = data;
                }
                let user_updated = await UserModel.findByIdAndUpdate(id, { $set: new_data }, { new: true })
                if (!user_updated) {
                    response = { success: false, message: 'User does not exists.' };
                } else {
                    response = { success: true, message: 'User profile update successfully', data: user_updated };
                }
            } else {
                response = { success: false, message: 'User is not valid.' };
            }
            resolve(response)
        } catch (err) {
            reject(err)
        }
    })
}

exports.getall_users = ({ query }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let { page, count, sort, order, name, status, email, mobileNo, userType } = query;
            let skip = page * count - count || 0;
            let limit = parseInt(count) || 10;
            order = order == "desc" ? -1 : 1;
            sort = { [sort || 'createdAt']: order };
            let querydata = {
                userType: { $ne: 'admin' },
                ...(name && { name: { $regex: ".*" + name + ".*", $options: 'i' } }),
                ...(status && { status }),
                ...(email && { email: { $regex: ".*" + email + ".*", $options: 'i' } }),
                ...(mobileNo && { mobileNo: { $regex: ".*" + mobileNo + ".*", $options: 'i' } }),
                ...(userType && { userType }),
            };
            let _count = await UserModel.countDocuments(querydata);
            let users = await UserModel.find(querydata)
                .skip(skip)
                .limit(limit)
                .sort(sort);
            resolve({ success: true, data: users, count: _count, message: 'All users fetched.' });
        } catch (err) {
            reject(err);
        }
    });
}

exports.get_user_details = ({ id }) => {
    return new Promise(async (resolve, reject) => {
        try {
            let user = await UserModel.findById(ObjectId(id));
            if (!user || user == null) {
                resolve({ success: false, message: 'User does not exist.' })
            } else {
                resolve({ success: true, data: user, message: 'User details returned successfully' });
            }
        } catch (err) {
            reject(err);
        }
    });
}