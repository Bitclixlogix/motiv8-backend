const userModel = require('../models/user')
const OTPModel = require('../models/otp')
const constants = require('../constants');
const { signin } = require('../services/jwt')

module.exports.otpVerify = ({ body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            const userCredentials = await userModel.findOne({ $and: [{ 'mobileNo': body.mobileNo }, { userType: 'User' }, { status: 'Active' }] })
            if (!userCredentials) {
                resolve({ success: false, message: 'Invalid User' })
            } else {
                let data = await otpVerify(userCredentials._id, body.otp);
                if (data !== true) {
                    resolve({ success: false, message: data })
                } else {
                    await userModel.findByIdAndUpdate(userCredentials._id, { $set: { isOtpVerified: true } })
                    resolve({
                        userCredentials,
                        token: signin({
                            id: userCredentials._id,
                            userType: userCredentials.userType,
                            name: userCredentials.name,
                            mobileNo: userCredentials.mobileNo,
                            profileImage: userCredentials.profileImage,
                            email: userCredentials.email
                        }),
                        success: true,
                        message: 'login successfully'
                    })
                }
            }
        } catch (error) {
            reject(error)
        }
    })
}


module.exports.login = ({ body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            const userCredentials = await userModel.findOneAndUpdate({ $and: [{ 'mobileNo': body.mobileNo }, { userType: 'User' }, { status: 'Active' }] }, { $set: { isOtpVerified: false } }, { new: true })
            let data = {}
            if (!userCredentials) {
                data = { success: false, message: 'Invalid User' }
            } else {
                let check = await OTPModel.findOne({ $and: [{ userid: userCredentials._id }, { type: 'User' }] });
                if (check) {
                    check.createdAt = new Date();
                    await check.save();
                } else {
                    let model = new OTPModel({ userid: userCredentials._id, type: 'User' });
                    await model.save();
                }
                data = { success: true, message: 'OTP sent to you registered mobile number' }
            }
            resolve(data)
        } catch (error) {
            reject(error)
        }
    })
}

exports.adminlogin = ({ body }) => {
    return new Promise(async (resolve, reject) => {
        try {
            const adminCredentials = await userModel.findOne({ $and: [{ email: body.email }, { userType: 'Admin' }, { status: 'Active' }] })
            if (!adminCredentials) {
                throw new Error('Invalid Admin')
            } else {
                let model = new userModel();
                let validatePass = await model.compare(body.password, adminCredentials.password);
                if (!validatePass) {
                    throw new Error('Invalid Credentials')
                }
                resolve({
                    adminCredentials: {
                        id: adminCredentials._id,
                        userType: 'Admin',
                        name: adminCredentials.name,
                        profileimage: adminCredentials.profileimage,
                        email: adminCredentials.email,
                        mobileNo: adminCredentials.mobileNo
                    },
                    token: signin({
                        id: adminCredentials._id,
                        userType: 'Admin',
                        name: adminCredentials.name,
                        profileimage: adminCredentials.profileimage,
                        email: adminCredentials.email,
                        mobileNo: adminCredentials.mobileNo
                    }, constants.jwtSecret, { expiresIn: constants.tokenExpiresIn })
                })
            }
        } catch (error) {
            console.log(error)
            reject(error)
        }
    });
}

async function otpVerify(_id, otp) {
    return new Promise(async (resolve, reject) => {
        try {
            let model = new OTPModel(); let message;
            let otpDB = await OTPModel.findOne({ $and: [{ userid: _id }, { type: 'User' }] });
            if (!otpDB) {
                message = 'Invalid OTP.';
            } else if (otpDB.otp != otp) {
                message = 'OTP is incorrect.';
            } else if (await model.isExpired(_id, 'User')) {
                await OTPModel.deleteOne({ userid: _id });
                message = 'OTP expired.';
            } else {
                await OTPModel.deleteOne({ userid: _id });
                message = true;
            }
            resolve(message);
        } catch (err) {
            reject(err);
        }
    })
}

