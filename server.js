require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const http = require('http');
dirName = __dirname;

const apiRouter = require('./src/routes/api');

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', apiRouter);
mongoose.Promise = global.Promise;
mongoose.set('useCreateIndex', true);
mongoose
  .connect(process.env.DATABASE_URL, { useNewUrlParser: true })
  .then(() => {
    console.log('Successfully connected to the database');
  })
  .catch(err => {
    console.log('Could not connect to the database. Exiting now...');
    process.exit(); // to close app Completely
  });

app.get('/', (req, res) => {
  console.log('object');
  res.json({ message: 'Welcome to Motiv8' });
});

var httpServer = http.createServer(app);
console.log(process.env.PORT);
httpServer.listen(process.env.PORT);

exports.closeServer = function () {
  httpServer.close();
};
